export const variables = {
  scale: 1,
  frameRate: 300,

  resetVelocity: { x: 6, y: 10 },

  field: {
    height: 500,
    width: 800,
  },

  ballRadius: 20,

  paddleSize: 0.3, // of wall size
  paddleSmoothness: 10, // default 10
  paddleSpeed: 1, // default 1

  multiplierPerHit: 3, // default 1, ball speeds up per hit

  winCondition: 7,

  controls: {
    resetInput: "space",
    p1Controls: {
      up: "w",
      down: "s",
    },
    p2Controls: {
      up: "up",
      down: "down",
    },
  },
}
