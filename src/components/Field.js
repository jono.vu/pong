import React from "react"

const Field = props => {
  const field = props.field
  const scale = props.scale
  const fieldWidth = field.width * scale
  const fieldHeight = field.height * scale
  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        width: fieldWidth,
        height: fieldHeight,
        background: "#111111",
        outline: "4px solid #222222",
      }}
    />
  )
}

export default Field
