import React from "react"

import { variables } from "../utils/variables"

const Ball = props => {
  const position = props.position
  const scale = props.scale
  const frameRate = props.frameRate
  const ballRadius = variables.ballRadius
  const ball = {
    size: ballRadius,
    color: "white",
  }
  return (
    <div
      style={{
        position: "absolute",
        // transition: frameRate < 11 && `all ${1 / frameRate}s linear`,
        top: position.y * scale - ball.size / 2,
        left: position.x * scale - ball.size / 2,
        width: ball.size,
        height: ball.size,
        // borderRadius: ball.size * 2,
        backgroundColor: ball.color,
      }}
    />
  )
}

export default Ball
