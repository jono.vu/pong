import React from "react"

import { variables } from "../utils/variables"

const Ui = props => {
  const message = props.message
  const score = props.score
  const onClick = props.onClick
  const field = variables.field
  return (
    <div
      style={{
        position: "absolute",
        zIndex: "10",
      }}
    >
      <span className="score">{score}</span>
      <h3
        className="message"
        style={{ width: field.width, marginTop: `${field.height / 2}px` }}
      >
        {message}
      </h3>
    </div>
  )
}

export default Ui
