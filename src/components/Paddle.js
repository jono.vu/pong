import React from "react"

const Paddle = props => {
  const frameRate = props.frameRate
  const p1 = props.p1 || null
  const p2 = props.p2 || null
  const p1Position = props.p1Position || null
  const p2Position = props.p2Position || null
  const paddleWidth = props.paddleWidth
  const paddle = {
    thickness: 20,
    height: paddleWidth,
    stroke: "black",
    color: "white",
  }
  return (
    <div
      style={{
        position: "absolute",
        // transition: `all 0.1s linear`,
        top: p1
          ? p1Position.y - paddle.height / 2
          : p2Position.y - paddle.height / 2,
        left: p1 ? p1Position.x - paddle.thickness : p2Position.x,
        width: paddle.thickness,
        height: paddle.height,
        background: paddle.color,
      }}
    />
  )
}

export default Paddle
