import React, { useState, useCallback, useEffect } from "react"

import { useInterval } from "../hooks/useInterval"

import KeyboardEventHandler from "react-keyboard-event-handler"

import { variables } from "../utils/variables"

import Field from "./Field"
import Ball from "./Ball"
import Ui from "./Ui.js"
import Paddle from "./Paddle.js"

const Pong = () => {
  const scale = variables.scale
  const frameRate = variables.frameRate
  const resetVelocity = variables.resetVelocity
  const field = variables.field
  const ballRadius = variables.ballRadius
  const paddleSize = variables.paddleSize
  const paddleSmoothness = 50 / variables.paddleSmoothness
  const paddleSpeed = variables.paddleSpeed
  const multiplierPerHit = variables.multiplierPerHit
  const winCondition = variables.winCondition - 2
  const p1Controls = variables.controls.p1Controls
  const p2Controls = variables.controls.p2Controls
  const resetInput = variables.controls.resetInput

  const wall = {
    left: 0,
    right: field.width,
    top: 0,
    bottom: field.height,
  }

  // Ball Physics

  const [ballPosition, setBallPosition] = useState({ x: 0, y: wall.bottom / 2 })
  const [ballVector, setBallVector] = useState({ x: 1, y: 0 })

  const applyVelocity = (position, vector) => {
    setBallPosition({
      x: position.x + vector.x,
      y: position.y + vector.y,
    })
    setBallVector({
      x: vector.x,
      y: vector.y,
    })
  }

  // Paddles

  const [p1Position, setP1Position] = useState({
    x: wall.left,
    y: wall.bottom / 2,
  })
  const [p2Position, setP2Position] = useState({
    x: wall.right,
    y: wall.bottom / 2,
  })

  const paddleWidth = wall.bottom * paddleSize

  const p1 = {
    lowerBoundary: p1Position.y - paddleWidth / 2 - ballRadius / 2,
    higherBoundary: p1Position.y + paddleWidth / 2 + ballRadius / 2,
  }
  const p2 = {
    lowerBoundary: p2Position.y - paddleWidth / 2 - ballRadius / 2,
    higherBoundary: p2Position.y + paddleWidth / 2 + ballRadius / 2,
  }

  const p1Hit = position => {
    if (p1.lowerBoundary < position.y && position.y < p1.higherBoundary)
      return true
    else {
      setMessage("Player 1 wins point! Press [ spacebar ] to start Round.")
      p2WinPoint(p2Points)
      setTimerRunning(false)
    }
  }
  const p2Hit = position => {
    if (p2.lowerBoundary < position.y && position.y < p2.higherBoundary)
      return true
    else {
      setMessage("Player 2 wins point! Press [ spacebar ] to start Round.")
      p1WinPoint(p1Points)
      setTimerRunning(false)
    }
  }

  // Controls

  const [p1SlideUp, setP1SlideUp] = useState(false)
  const [p1SlideDown, setP1SlideDown] = useState(false)

  const [p2SlideUp, setP2SlideUp] = useState(false)
  const [p2SlideDown, setP2SlideDown] = useState(false)

  useInterval(
    () => {
      moveP1(p1SlideUp && p1Controls.up)
    },
    p1SlideUp ? paddleSmoothness : null
  )

  useInterval(
    () => {
      moveP1(p1SlideDown && p1Controls.down)
    },
    p1SlideDown ? paddleSmoothness : null
  )

  useInterval(
    () => {
      moveP2(p2SlideUp && p2Controls.up)
    },
    p2SlideUp ? paddleSmoothness : null
  )

  useInterval(
    () => {
      moveP2(p2SlideDown && p2Controls.down)
    },
    p2SlideDown ? paddleSmoothness : null
  )

  const buffer = (paddleSpeed * wall.bottom) / 100

  const moveP1 = key => {
    if (key === p1Controls.up && p1Position.y < paddleWidth / 2 + buffer) {
      setP1Position({
        x: p1Position.x,
        y: paddleWidth / 2,
      })
    } else if (
      key === p1Controls.up &&
      p1Position.y <= wall.bottom + paddleWidth / 2
    ) {
      setP1Position({
        x: p1Position.x,
        y: p1Position.y - buffer,
      })
    } else if (
      key === p1Controls.down &&
      p1Position.y > wall.bottom - paddleWidth / 2 - buffer
    ) {
      setP1Position({
        x: p1Position.x,
        y: wall.bottom - paddleWidth / 2,
      })
    } else if (
      key === p1Controls.down &&
      p1Position.y <= wall.bottom - paddleWidth / 2
    ) {
      setP1Position({
        x: p1Position.x,
        y: p1Position.y + buffer,
      })
    }
  }

  const moveP2 = key => {
    if (key === p2Controls.up && p2Position.y < paddleWidth / 2 + buffer) {
      setP2Position({
        x: p2Position.x,
        y: paddleWidth / 2,
      })
    } else if (
      key === p2Controls.up &&
      p2Position.y <= wall.bottom + paddleWidth / 2
    ) {
      setP2Position({
        x: p2Position.x,
        y: p2Position.y - buffer,
      })
    } else if (
      key === p2Controls.down &&
      p2Position.y > wall.bottom - paddleWidth / 2 - buffer
    ) {
      setP2Position({
        x: p2Position.x,
        y: wall.bottom - paddleWidth / 2,
      })
    } else if (
      key === p2Controls.down &&
      p2Position.y <= wall.bottom - paddleWidth / 2
    ) {
      setP2Position({
        x: p2Position.x,
        y: p2Position.y + buffer,
      })
    }
  }

  // Collision

  const [p1PaddleSpeed, setP1PaddleSpeed] = useState(0)
  const [p2PaddleSpeed, setP2PaddleSpeed] = useState(0)

  const bounceRightWall = (position, vector) => {
    return {
      position: { x: wall.right, y: position.y },
      vector: {
        x: -vector.x * multiplierPerHit - p2PaddleSpeed * 0.5,
        y: vector.y * multiplierPerHit + p2PaddleSpeed,
      },
    }
  }
  const bounceLeftWall = (position, vector) => {
    return {
      position: { x: wall.left, y: position.y },
      vector: {
        x: -vector.x * multiplierPerHit - p2PaddleSpeed * 0.5,
        y: vector.y * multiplierPerHit + p1PaddleSpeed,
      },
    }
  }

  const bounceBottomWall = (position, vector) => {
    return {
      position: { x: position.x, y: wall.bottom },
      vector: { x: vector.x, y: -vector.y },
    }
  }
  const bounceTopWall = (position, vector) => {
    return {
      position: { x: position.x, y: wall.top },
      vector: { x: vector.x, y: -vector.y },
    }
  }

  const checkCollision = (position, vector) => {
    //
    if (position.x > wall.right && p2Hit(position))
      return bounceRightWall(position, vector)
    //
    else if (position.x < wall.left && p1Hit(position))
      return bounceLeftWall(position, vector)
    //
    else if (position.y > wall.bottom) return bounceBottomWall(position, vector)
    //
    else if (position.y < wall.top) return bounceTopWall(position, vector)
    //
    else return { position, vector }
  }

  // Timer

  const [time, setTime] = useState(0)
  const [timerRunning, setTimerRunning] = useState(false)

  useInterval(
    async () => {
      setTime(time + 1)
      const res = await checkCollision(ballPosition, ballVector)
      applyVelocity(res.position, res.vector)
      // status()
    },
    timerRunning ? 1000 / frameRate : null
  )

  const reset = () => {
    setBallPosition({ x: 0, y: wall.bottom / 2 })
    setBallVector(resetVelocity)
  }

  const handleReset = async () => {
    if (p2Points > winCondition + 1 || p1Points > winCondition + 1) {
      setP1Points(0)
      setP2Points(0)
      setMessage("")
      setP1PaddleSpeed(0)
      setP2PaddleSpeed(0)
      setTime(false)
      reset()
      setTimerRunning(true)
    } else {
      setMessage("")
      setP1PaddleSpeed(0)
      setP2PaddleSpeed(0)
      setTime(false)
      reset()
      setTimerRunning(true)
    }
  }

  // Messages

  const [message, setMessage] = useState(
    "PONG! Press [ spacebar ] to start Round."
  )

  const status = () =>
    console.log({
      time: time,
      ballPositionX: ballPosition.x,
      ballPositionY: ballPosition.y,
      ballVectorX: ballVector.x,
      ballVectorY: ballVector.y,
    })

  // Points

  const [p1Points, setP1Points] = useState(0)
  const [p2Points, setP2Points] = useState(0)

  const p1WinPoint = async points => {
    if (points > winCondition) {
      setP1Points(points + 1)
      setMessage(
        `Player 1 Win! { ${p1Points +
          1} : ${p2Points} } Press [ spacebar ] to start a new game.`
      )
    } else {
      setP1Points(points + 1)
    }
  }

  const p2WinPoint = async points => {
    if (points > winCondition) {
      setP2Points(points + 1)
      setMessage(
        `Player 2 Win! { ${p1Points} : ${p2Points +
          1} } Press [ spacebar ] to start a new game.`
      )
    } else {
      setP2Points(points + 1)
    }
  }

  const score = `${p1Points} : ${p2Points}`

  return (
    <>
      <Ui message={message} score={score} onClick={() => handleReset()} />
      {typeof window != undefined && (
        <>
          {!timerRunning && (
            <KeyboardEventHandler
              handleKeys={[resetInput]}
              onKeyEvent={(key, e) => {
                handleReset()
              }}
            />
          )}
          <KeyboardEventHandler
            handleKeys={[p1Controls.up, p1Controls.down]}
            onKeyEvent={(key, e) => {
              if (key === p1Controls.up) {
                setP1SlideUp(true)
                setP1PaddleSpeed(-p1PaddleSpeed - 2)
              } else if (key === p1Controls.down) {
                setP1SlideDown(true)
                setP1PaddleSpeed(p1PaddleSpeed + 2)
              }
            }}
          />
          <KeyboardEventHandler
            handleKeys={[p1Controls.up, p1Controls.down]}
            handleEventType="keyup"
            onKeyEvent={(key, e) => {
              if (key === p1Controls.up) {
                setP1SlideUp(false)
                setP1PaddleSpeed(0)
              } else if (key === p1Controls.down) {
                setP1SlideDown(false)
                setP1PaddleSpeed(0)
              }
            }}
          />
          <KeyboardEventHandler
            handleKeys={[p2Controls.up, p2Controls.down]}
            onKeyEvent={(key, e) => {
              if (key === p2Controls.up) {
                setP2SlideUp(true)
                setP2PaddleSpeed(-p2PaddleSpeed - 2)
              } else if (key === p2Controls.down) {
                setP2SlideDown(true)
                setP2PaddleSpeed(p1PaddleSpeed + 2)
              }
            }}
          />
          <KeyboardEventHandler
            handleKeys={[p2Controls.up, p2Controls.down]}
            handleEventType="keyup"
            onKeyEvent={(key, e) => {
              if (key === p2Controls.up) {
                setP2SlideUp(false)
                setP2PaddleSpeed(0)
              } else if (key === p2Controls.down) {
                setP2SlideDown(false)
                setP2PaddleSpeed(0)
              }
            }}
          />
        </>
      )}
      <Field field={field} scale={scale} />
      <Ball position={ballPosition} frameRate={frameRate} scale={scale} />
      <Paddle
        p1={p1}
        p1Position={p1Position}
        paddleWidth={paddleWidth}
        frameRate={frameRate}
      />
      <Paddle
        p2={p2}
        p2Position={p2Position}
        paddleWidth={paddleWidth}
        frameRate={frameRate}
      />
    </>
  )
}

export default Pong
