import React from "react"

import Pong from "../components/Pong"

import { variables } from "../utils/variables"

import "../index.scss"

export default () => {
  const field = variables.field
  const styles = {
    container: {
      display: "flex",
      width: "100vw",
      height: "100vh",
      backgroundColor: "black",
      justifyContent: "center",
    },
    wrapper: {
      position: "relative",
      margin: "auto",
      background: "white",
      display: "block",
      width: field.width,
      marginTop: `calc(50vh - ${field.height / 2}px)`,
    },
  }
  return (
    <div style={styles.container}>
      <div style={styles.wrapper}>
        <Pong />
      </div>
    </div>
  )
}
